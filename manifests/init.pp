# Installs ZFS on linux
# Currently only tested on CentOS7
class zfsonlinux {
  require ::zfsonlinux::repo
  ensure_packages(
    ['zfs'],
    { ensure => latest })
}
