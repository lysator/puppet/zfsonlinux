# Adds CentOS 7 repo for zfs on linux.
# Sets it to use the kmod module instead of the dkms module, so we
# don't have to build it ourselves.
class zfsonlinux::repo {
  package { 'zfs-release':
    ensure   => present,
    provider => rpm,
    source   => 'http://download.zfsonlinux.org/epel/zfs-release.el7_7.noarch.rpm',
  } ~> exec { 'validate gpg key':
    path      => '/bin:/usr/bin:/sbin:/usr/sbin',
    command   => 'gpg --quiet --with-fingerprint /etc/pki/rpm-gpg/RPM-GPG-KEY-zfsonlinux | grep -q "C93A FFFD 9F3F 7B03 C310  CEB6 A9D5 A1C0 F14A B620"',
    logoutput => 'on_failure',
  }

  ini_setting { 'Disable ZFS DKMS':
    ensure  => present,
    path    => '/etc/yum.repos.d/zfs.repo',
    section => 'zfs',
    setting => 'enabled',
    value   => '0',
    require => [ Package['zfs-release'], ],
  }

  ini_setting { 'Enable ZFS KMOD':
    ensure  => present,
    path    => '/etc/yum.repos.d/zfs.repo',
    section => 'zfs-kmod',
    setting => 'enabled',
    value   => '1',
    require => [ Package['zfs-release'], ],
  }

}
